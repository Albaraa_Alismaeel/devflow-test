import { authMiddleware } from '@clerk/nextjs/server';

export default authMiddleware({
  // "/" will be accessible to all users
  publicRoutes: [
    '/',
    '/api/webhooks(.*)',
    '/tags',
    '/profile/:id',
    '/community',
    // '/jobs',
  ],
  ignoredRoutes: ['/api/webhooks(.*)', '/api/webhoks'],
});

export const config = {
  matcher: [
    '/((?!.+\\.[\\w]+$|_next).*)',
    '/((?!.*\\..*|_next).*)',
    '/',
    '/(api|trpc)(.*)',
  ],
};
