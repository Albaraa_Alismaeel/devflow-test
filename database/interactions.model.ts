import { Document, Schema, model, models } from 'mongoose';

export interface IInteractions extends Document {
  user: Schema.Types.ObjectId;
  actions: string;
  question: Schema.Types.ObjectId;
  answer: Schema.Types.ObjectId;
  tags: Schema.Types.ObjectId[];
  createdAt: Date;
}

const InteractionsSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User', required: true},
  actions: { type: String, required: true },
  question: { type: Schema.Types.ObjectId, ref: 'Question' },
  answer: { type: Schema.Types.ObjectId, ref: 'Answer' },
  tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
  createdAt: { type: Date, default: Date.now },
});

const Interactions = models.Interactions || model('Interactions', InteractionsSchema);

export default Interactions;