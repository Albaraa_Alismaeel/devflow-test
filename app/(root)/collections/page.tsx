import QuestionCard from '@/components/cards/question';
import NoResult from '@/components/shared/no-result';
import PageHead from '@/components/shared/page-head';
import Pagination from '@/components/shared/pagination';

import { QuestionFilters } from '@/constants/filters';
import { getSavedQuestions } from '@/lib/actions/user.action';
import { SearchParamsProps } from '@/types';
import { auth } from '@clerk/nextjs/server';
import { redirect } from 'next/navigation';

const Home = async ({ searchParams }: SearchParamsProps) => {
  const { userId } = auth();
  if (!userId) redirect('/sign-up');

  const { savedQuestions: questions, isNext } = (await getSavedQuestions({
    clerkId: userId,
    searchQuery: searchParams.q,
    filter: searchParams.f,
    page: searchParams.page ? +searchParams.page : 1,
    pageSize: 10
  })) as { savedQuestions: any; isNext: boolean };

  return (
    <section>
      <PageHead
        title="Saved Questions"
        filters={QuestionFilters}
        placeholder="Search for Questions Here..."
      />
      <div className="mt-10 flex w-full flex-col gap-6">
        {questions.length > 0 ? (
          questions.map((question: any) => (
            <QuestionCard
              key={question._id}
              question={question}
              isSaved={true}
              isEditable={true}
            />
          ))
        ) : (
          <NoResult
            title="There's no question saved to show"
            desc="Be the first to break the silence! 🚀 Ask a Question and kickstart the discussion. our query could be the next big thing others learn from. Get involved! 💡"
            withImg={true}
            action="Ask a Question"
            route="/ask-question"
          />
        )}
      </div>

      <Pagination
        pageNumber={searchParams?.page ? +searchParams.page : 1}
        isNext={isNext}
      />
    </section>
  );
};

export default Home;
