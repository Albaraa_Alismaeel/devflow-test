import TagCard from '@/components/cards/tag';
import NoResult from '@/components/shared/no-result';
import PageHead from '@/components/shared/page-head';
import Pagination from '@/components/shared/pagination';
import { TagFilters } from '@/constants/filters';
import { getAllTags } from '@/lib/actions/tag.actions';
import { SearchParamsProps } from '@/types';

const Tags = async ({ searchParams }: SearchParamsProps) => {
  const { tags, isNext } = await getAllTags({
    searchQuery: searchParams.q,
    filter: searchParams.f,
    page: searchParams.page ? +searchParams.page : 1,
    pageSize: 15,
  });

  return (
    <section>
      <PageHead
        title="Tags"
        filters={TagFilters}
        placeholder="Search by tag name..."
      />
      {tags.length === 0 ? (
        <NoResult
          title="No Tags Yet"
          desc="No one has added tags yet...😓"
          action="Be the first to add"
          route="/ask-question"
        />
      ) : (
        <div className="mt-10 grid w-full gap-x-3 gap-y-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-2 2xl:grid-cols-3 ">
          {tags.map((tag) => (
            <TagCard
              key={tag._id}
              id={tag._id}
              tag={tag.name}
              questions={tag.questions.length}
            />
          ))}
        </div>
      )}

      <Pagination
        pageNumber={searchParams?.page ? +searchParams.page : 1}
        isNext={isNext}
      />
    </section>
  );
};

export default Tags;
