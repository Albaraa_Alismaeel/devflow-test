import QuestionCard from '@/components/cards/question';
import NoResult from '@/components/shared/no-result';
import PageHead from '@/components/shared/page-head';
import Pagination from '@/components/shared/pagination';
import { GetQuestionsByTagId } from '@/lib/actions/tag.actions';

interface Props {
  params: { id: string };
  searchParams: { [key: string]: string | undefined  };
}

const TagDetails = async ({ params: { id }, searchParams }: Props) => {
  const { tagTitle, questions , isNext} = await GetQuestionsByTagId({
    tagId: id,
    searchQuery: searchParams.q,
    page: searchParams.page ? +searchParams.page : 1,
    pageSize: 10
  });

  return (
    <section>
      <PageHead
        title={tagTitle}
        placeholder="Search for question here..."
        withFilter={false}
      />
      <div className="mt-10 flex w-full flex-col gap-6">
        {questions.length > 0 ? (
          questions.map((question: any) => (
            <QuestionCard key={question.id} question={question} />
          ))
        ) : (
          <NoResult
            title="There's no tag questions to show"
            desc="Be the first to break the silence! 🚀 Ask a Question and kickstart the discussion. our query could be the next big thing others learn from. Get involved! 💡"
            withImg={true}
            action="Ask a Question"
            route="/ask-question"
          />
        )}
      </div>
      <Pagination
        pageNumber={searchParams?.page ? +searchParams.page : 1}
        isNext={isNext}
      />
    </section>
  );
};

export default TagDetails;

