import QuestionCard from '@/components/cards/question';
import FilterTags from '@/components/home/filter-tags';
import NoResult from '@/components/shared/no-result';
import PageHead from '@/components/shared/page-head';
import Pagination from '@/components/shared/pagination';

import { HomePageFilters } from '@/constants/filters';
import {
  getQuestions,
  getRecommendedQuestions,
} from '@/lib/actions/question.action';
import { SearchParamsProps } from '@/types';
import { auth } from '@clerk/nextjs/server';

const Home = async ({ searchParams }: SearchParamsProps) => {
  const { userId } = auth();

  let result;

  if (searchParams?.filter === 'recommended') {
    if (userId) {
      result = (await getRecommendedQuestions({
        userId,
        searchQuery: searchParams.q,
        page: searchParams.page ? +searchParams.page : 1,
        pageSize: 10,
      })) as any;
    } else {
      result = {
        questions: [],
        isNext: false,
      };
    }
  } else {
    result = (await getQuestions({
      searchQuery: searchParams.q,
      filter: searchParams.f,
      page: searchParams.page ? +searchParams.page : 1,
      pageSize: 10,
    })) as any;
  }

  return (
    <section>
      <PageHead
        title="All Questions"
        filters={HomePageFilters}
        placeholder="Search for Questions Here..."
        askButton={true}
      />
      <FilterTags />
      <div className="mt-10 flex w-full flex-col gap-6">
        {result?.questions.length > 0 ? (
          result?.questions.map((question: any) => (
            <QuestionCard key={question.id} question={question} />
          ))
        ) : (
          <NoResult
            title="There's no questions to show"
            desc="Be the first to break the silence! 🚀 Ask a Question and kickstart the discussion. our query could be the next big thing others learn from. Get involved! 💡"
            withImg={true}
            action="Ask a Question"
            route="/ask-question"
          />
        )}
      </div>

      <Pagination
        pageNumber={searchParams?.page ? +searchParams.page : 1}
        isNext={result?.isNext}
      />
    </section>
  );
};

export default Home;
