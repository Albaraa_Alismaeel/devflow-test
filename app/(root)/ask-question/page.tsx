import Question from '@/components/forms/question';
import { getUserById } from '@/lib/actions/user.action';
import { auth } from '@clerk/nextjs/server';
import { redirect } from 'next/navigation';

const AskQuestion = async () => {
  const { userId } = auth();
  if (!userId) redirect('/sign-in');

  const user = await getUserById({ userId });

  return (
    <div>
      <h1 className="h1-bold text-dark100_light900">Ask a puplic question</h1>
      <div className="mt-9">
        <Question type="create" userId={JSON.stringify(user._id)} />
      </div>
    </div>
  );
};

export default AskQuestion;
