import ProfileForm from '@/components/forms/profile';
import { getUserById } from '@/lib/actions/user.action';

const EditProfile = async ({ params }: { params: { id: string } }) => {
  const user = await getUserById({ userId: params.id });

  return (
    <div>
      <h1 className="h1-bold text-dark100_light900">Edit Profile</h1>
      <div className="mt-9">
        <ProfileForm
          clerkId={user.clerkId}
          name={user.name}
          username={user.username}
          portfolio={user?.portfolioWebsite}
          location={user?.location}
          bio={user?.bio}
        />
      </div>
    </div>
  );
};

export default EditProfile;
