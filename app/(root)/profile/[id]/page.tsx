import Stats from '@/components/shared/Stats';
import AnswersTab from '@/components/shared/tabs/answers';
import QuestionsTab from '@/components/shared/tabs/questions';
import { Button } from '@/components/ui/button';
import ProfileLink from '@/components/ui/profile-link';
import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { getUserInfo } from '@/lib/actions/user.action';
import { getJoineDate } from '@/lib/utils';
import { SearchParamsProps } from '@/types';
import { auth } from '@clerk/nextjs/server';
import Image from 'next/image';
import Link from 'next/link';

interface Props extends SearchParamsProps {
  params: { id: string };
}
const Profile = async ({ params, searchParams }: Props) => {
  const { userId: ClerkId } = auth();
  const { user, totalQuestions, totalAnswer, badges } = await getUserInfo({
    userId: params.id,
  });

  return (
    <section>
      <div className="flex flex-col-reverse items-start justify-between sm:flex-row">
        <div className="flex w-full flex-col items-start gap-4 lg:flex-row">
          <Image
            src={user?.picture}
            alt={user?.name}
            width={140}
            height={140}
            className="size-[140px] rounded-full object-cover"
          />
          <div className="mt-3 grow">
            <div className="flex items-start justify-between gap-4">
              <div>
                <h2 className="h2-bold text-dark100_light900">
                  {user.name}
                </h2>
                <p className="paragraph-regular text-dark200_light800">
                  @{user.username}
                </p>
              </div>
              <div className="flex justify-end max-sm:mb-5 max-sm:w-full">
                {ClerkId === params.id && (
                  <Link href={`/profile/edit/${params.id}`}>
                    <Button className="paragraph-medium btn-secondary text-dark300_light900 min-h-[46px] min-w-[175px] px-4 py-3">
                      Edit Profile
                    </Button>
                  </Link>
                )}
              </div>
            </div>
            <div className="mt-5 flex flex-wrap items-center justify-start gap-5">
              {user.portfolioWebsite && (
                <ProfileLink
                  imgUrl="/assets/icons/link.svg"
                  href={user.portfolioWebsite}
                  title="Portfolio"
                />
              )}
              {user.location && (
                <ProfileLink
                  imgUrl="/assets/icons/location.svg"
                  title={user.location}
                />
              )}
              <ProfileLink
                imgUrl="/assets/icons/calendar.svg"
                title={getJoineDate(user.joinedAt)}
              />
            </div>
            {user.bio && (
              <p className="paragraph-regular text-dark400_light800 mt-8">
                {user.bio}
              </p>
            )}
          </div>
        </div>
      </div>
      <Stats
        reputation={user.reputation}
        totalQuestions={totalQuestions}
        totalAnswer={totalAnswer}
        goldBadges={badges.GOLD}
        silverBadges={badges.SILVER}
        bronzeBadges={badges.BRONZE}
      />
      <div className="mt-10 flex gap-10">
        <Tabs defaultValue="top-posts" className="flex-1">
          <TabsList className="background-light800_dark400 min-h-[42px] p-1">
            <TabsTrigger className="tab" value="top-posts">
              Top Posts
            </TabsTrigger>
            <TabsTrigger className="tab" value="answers">
              Answers
            </TabsTrigger>
          </TabsList>
          <TabsContent value="top-posts">
            <QuestionsTab
              userId={user._id}
              searchParams={searchParams}
            />
          </TabsContent>
          <TabsContent value="answers">
            <AnswersTab
              userId={user._id}
              searchParams={searchParams}
            />
          </TabsContent>
        </Tabs>
      </div>
    </section>
  );
};

export default Profile;
