import UserCard from '@/components/cards/user';
import NoResult from '@/components/shared/no-result';
import PageHead from '@/components/shared/page-head';
import Pagination from '@/components/shared/pagination';
import { UserFilters } from '@/constants/filters';
import { getAllUsers } from '@/lib/actions/user.action';
import { SearchParamsProps } from '@/types';

const Community = async ({ searchParams }: SearchParamsProps) => {
  const { users, isNext } = await getAllUsers({
    searchQuery: searchParams.q,
    filter: searchParams.f,
    page: searchParams.page ? +searchParams.page : 1,
    pageSize: 3,
  });

  return (
    <section>
      <PageHead
        title="Our Community"
        filters={UserFilters}
        placeholder="Search by username..."
      />
      {users.length === 0 ? (
        <NoResult
          title="No Users Yet"
          desc="Our community is empty now...😓"
          action="Join Us"
          route="sign-up"
        />
      ) : (
        <div className="mt-10 grid w-full gap-x-3 gap-y-6 md:grid-cols-2 2xl:grid-cols-3 ">
          {users.map((user) => (
            <UserCard
              key={user._id}
              _id={user._id}
              clerkId={user.clerkId}
              name={user.name}
              picture={user.picture}
              avatarFallback={user.name[0] + user.name[1]}
              username={user.username}
            />
          ))}
        </div>
      )}

      <Pagination
        pageNumber={searchParams?.page ? +searchParams.page : 1}
        isNext={isNext}
      />
    </section>
  );
};

export default Community;
