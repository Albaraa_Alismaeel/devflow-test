import Question from '@/components/forms/question';
import { getQuestionById } from '@/lib/actions/question.action';
import { getUserById } from '@/lib/actions/user.action';
import { auth } from '@clerk/nextjs/server';
import { redirect } from 'next/navigation';

const EditQuestion = async ({ params }: { params: { id: string } }) => {
  const { userId } = auth();
  if (!userId) redirect('/sign-in');

  const user = await getUserById({ userId });
  const question = await getQuestionById({ questionId: params.id });

  return (
    <div>
      <h1 className="h1-bold text-dark100_light900">Edit a question</h1>
      <div className="mt-9">
        <Question
          userId={JSON.stringify(user._id)}
          question={JSON.stringify(question)}
          type="edit"
        />
      </div>
    </div>
  );
};

export default EditQuestion;
