import Answer from '@/components/forms/answer';
import Votes from '@/components/question/Votes';
import AnswerBox from '@/components/question/answer';
import Filter from '@/components/shared/filter';
import ParesHTML from '@/components/shared/parse-html';
import { Avatar, AvatarImage } from '@/components/ui/avatar';
import Metric from '@/components/ui/metric';
import TagBtn from '@/components/ui/tag-btn';
import { AnswerFilters } from '@/constants/filters';
import { getAnswers } from '@/lib/actions/answer.actoin';
import { viewQuestion } from '@/lib/actions/interactions.action';
import { getQuestionById } from '@/lib/actions/question.action';
import { getUserById } from '@/lib/actions/user.action';
import { getTimestamp } from '@/lib/utils';
import { auth } from '@clerk/nextjs/server';
import Link from 'next/link';
import { notFound } from 'next/navigation';

const QuestionDetails = async ({
  params: { id },
  searchParams,
}: {
  params: { id: string };
  searchParams: { f: string | undefined };
}) => {
  const question = await getQuestionById({ questionId: id });
  if (!question) notFound();
  const { userId } = auth();
  let user: any;

  if (userId) user = await getUserById({ userId });

  const answers = await getAnswers({ questionId: id, sortBy: searchParams.f });

  viewQuestion({
    questionId: id,
    userId: user._id ? user._id : undefined,
  });

  return (
    <section>
      {/* Question Header : ( Actions, Author, Title ) */}
      <div className="flex-start w-full flex-col">
        <div className="flex w-full flex-col-reverse justify-between gap-5 sm:flex-row sm:items-center sm:gap-2">
          <Link
            href={`/profile/${question.author.clerkId}`}
            className="flex items-center justify-start gap-1"
          >
            <Avatar className="size-6">
              <AvatarImage src={question.author.picture}></AvatarImage>
            </Avatar>
            <p className="paragraph-semibold text-dark300_light700">
              {question.author.name}
            </p>
          </Link>
          <div className="flex justify-end">
            <Votes
              type="question"
              itemId={JSON.stringify(question._id)}
              userId={JSON.stringify(user._id)}
              upvotes={question.upvotes.length}
              downvotes={question.downvotes.length}
              upvoted={question.upvotes.includes(user._id)}
              downvoted={question.downvotes.includes(user._id)}
              started={user.saved.includes(question._id)}
            />
          </div>
        </div>
        <h2 className="h2-semibold text-dark200_light900 mt-3.5 w-full text-left">
          {question.title}
        </h2>
      </div>
      {/* Statistics Section */}
      <div className="mb-8 mt-5 flex flex-wrap gap-4">
        <Metric
          imgUrl="/assets/icons/clock.svg"
          alt="clock"
          value="Asked"
          title={getTimestamp(question.createdAt)}
        />

        <Metric
          imgUrl="/assets/icons/message.svg"
          alt="message"
          value={question.answers.length}
          title="Answers"
        />

        <Metric
          imgUrl="/assets/icons/eye.svg"
          alt="eye"
          value={question.views}
          title="Views"
        />
      </div>
      {/* Question Content */}
      <ParesHTML data={question.content} />
      {/* Tags Section */}
      <div className="mt-8 flex flex-wrap gap-2">
        {question.tags.map((tag: any) => (
          <Link
            key={tag._id}
            href={`/tags/${tag._id}`}
            className="flex justify-between gap-2"
          >
            <TagBtn title={tag.name} />
          </Link>
        ))}
      </div>
      {/* Answers Section */}
      <div className="mt-11">
        <div className="flex items-center justify-between">
          <h3 className="primary-text-gradient">
            {question.answers.length} Answers
          </h3>
          <Filter filters={AnswerFilters} />
        </div>
        <div>
          {answers?.map((answer: any) => (
            <AnswerBox
              clerkId={answer.author.clerkId}
              key={answer._id}
              _id={answer._id}
              userId={user._id}
              name={answer.author.name}
              picture={answer.author.picture}
              content={answer.content}
              upvotes={answer.upvotes}
              downvotes={answer.downvotes}
              createdAt={getTimestamp(answer.createdAt)}
            />
          ))}
        </div>
        <div className="my-10 w-full"></div>
        <Answer
          userId={JSON.stringify(user._id)}
          questionId={JSON.stringify(id)}
        />
      </div>
    </section>
  );
};

export default QuestionDetails;
