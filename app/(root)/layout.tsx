import LeftSidebar from '@/components/shared/left-sidebar';
import Navbar from '@/components/shared/navbar';
import RightSidebar from '@/components/shared/right-sidebar';
import { Toaster } from '@/components/ui/toaster';
import React from 'react';

const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <main className="background-light850_dark100 relative">
      <Navbar />
      <div className="flex ">
        <LeftSidebar />
        <section className="flex min-h-screen max-w-full flex-1 flex-col px-6 pb-6 pt-36 max-md:pb-14  ">
          <div className="mx-auto w-full max-w-5xl">{children}</div>
        </section>
        <RightSidebar />
      </div>
      {/* Toster */}
      <Toaster />
    </main>
  );
};
export default Layout;
