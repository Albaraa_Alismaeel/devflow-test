import { BADGE_CRITERIA } from '@/constants';
import { BadgeCounts } from '@/types';
import { clsx, type ClassValue } from 'clsx';
import qs from 'query-string';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const getTimestamp = (createdAt: Date): string => {
  const currentTime = new Date();
  const timeDifference = currentTime.getTime() - createdAt.getTime();
  const secondsDifference = Math.floor(timeDifference / 1000);

  const intervals = {
    year: 31536000,
    month: 2592000,
    week: 604800,
    day: 86400,
    hour: 3600,
    minute: 60,
    second: 1,
  };

  for (const [interval, seconds] of Object.entries(intervals)) {
    const intervalCount = Math.floor(secondsDifference / seconds);
    if (intervalCount >= 1) {
      return `${intervalCount} ${interval}${intervalCount !== 1 ? 's' : ''} ago`;
    }
  }

  return 'Just now';
};

export const formatBigNumber = (number: number): string => {
  if (Math.abs(number) >= 1000000) {
    return (number / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
  } else if (Math.abs(number) >= 1000) {
    return (number / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
  } else {
    return number.toString();
  }
};

export const getJoineDate = (date: Date) => {
  const month = date.toLocaleString('default', { month: 'long' });
  const year = date.getFullYear();
  const joinedDate = `${month} ${year}`;
  return joinedDate;
};

type FormUrlParams = {
  params: string;
  key: string;
  value: string;
};

export const formUrlQuery = ({ params, key, value }: FormUrlParams) => {
  const currentUrl = qs.parse(params);
  currentUrl[key] = value;

  return qs.stringifyUrl(
    {
      url: window.location.pathname,
      query: currentUrl,
    },
    { skipNull: true }
  );
};

export const removeKeyesFromQuery = ({
  params,
  keys,
}: {
  params: string;
  keys: string[];
}) => {
  const currentUrl = qs.parse(params);

  keys.forEach((key) => {
    delete currentUrl[key];
  });

  return qs.stringifyUrl(
    {
      url: window.location.pathname,
      query: currentUrl,
    },
    { skipNull: true }
  );
};

type BadgeParam = {
  criteria: {
    type: keyof typeof BADGE_CRITERIA;
    count: number;
  }[];
};

export const assignBadge = (params: BadgeParam) => {
  const badgeCounts: BadgeCounts = {
    GOLD: 0,
    SILVER: 0,
    BRONZE: 0,
  };

  params.criteria.forEach((criterion) => {
    const { type, count } = criterion;
    const badgeCriteria: any = BADGE_CRITERIA[type];
    Object.keys(badgeCriteria).forEach((key: any) => {
      if (count >= badgeCriteria[key]) {
        badgeCounts[key as keyof BadgeCounts]++;
      }
    })
  });
  return badgeCounts;
};
