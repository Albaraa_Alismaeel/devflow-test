'use server';

import Interactions from '@/database/interactions.model';
import Question from '@/database/question.model';
import { connectToDatabase } from '../mongoose';
import { ViewQuestionParams } from './shared.types';

export async function viewQuestion(params: ViewQuestionParams) {
  try {
    connectToDatabase();

    const { questionId, userId } = params;

    await Question.findByIdAndUpdate(questionId, {
      $inc: { views: 1 },
    });
    
    if (userId) {
      const existingInteraction = await Interactions.findOne({
        user: userId,
        question: questionId,
        actions: 'view',
      });

      if (!existingInteraction) {
        await Interactions.create({
          user: userId,
          question: questionId,
          actions: 'view',
        });
      } 
    }
  } catch (error) {
    console.log(error);
  }
}
