import Question from '@/database/question.model';
import Tag, { ITag } from '@/database/tag.model';
import User from '@/database/user.model';
import { FilterQuery } from 'mongoose';
import { connectToDatabase } from '../mongoose';
import {
  GetAllTagsParams,
  GetQuestionsByTagIdParams,
  GetTopInteractedTagsParams,
} from './shared.types';

export async function getAllTags(params: GetAllTagsParams) {
  try {
    connectToDatabase();

    const { searchQuery, filter, page = 1, pageSize = 15 } = params;

    const query: FilterQuery<typeof Tag> = {};

    let sortOptions = {};

    switch (filter) {
      case 'popular':
        sortOptions = { questions: -1 };
        break;
      case 'recent':
        sortOptions = { createdAt: -1 };
        break;
      case 'name':
        sortOptions = { name: 1 };
        break;
      case 'old':
        sortOptions = { createdAt: 1 };
        break;
    }

    if (searchQuery) {
      query.$or = [{ name: { $regex: new RegExp(searchQuery, 'i') } }];
    }

    const tags = await Tag.find(query)
      .skip((page - 1) * pageSize)
      .limit(pageSize)
      .sort(sortOptions);

    const totalTags = await Tag.countDocuments(query);

    const isNext = totalTags > page * pageSize;

    return { tags, isNext };
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function getTopInteractedTags(params: GetTopInteractedTagsParams) {
  try {
    connectToDatabase();

    const { userId } = params;

    const user = await User.findById(userId);
    if (!user) throw new Error('User not found');

    return ['React', 'Node.js', 'JavaScript'];
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function GetQuestionsByTagId(params: GetQuestionsByTagIdParams) {
  try {
    connectToDatabase();

    const { tagId, page = 1, pageSize = 10, searchQuery } = params;

    const tagFilter: FilterQuery<ITag> = { _id: tagId };

    const questionsCount = await Tag.findOne(tagFilter).select('questions');

    const tag = await Tag.findOne(tagFilter).populate({
      path: 'questions',
      model: Question,
      match: searchQuery
        ? { title: { $regex: searchQuery, $options: 'i' } }
        : {},
      options: {
        sort: { createdAt: -1 },
        skip: (page - 1) * pageSize,
        limit: pageSize,
      },
      populate: [
        {
          path: 'author',
          model: User,
          select: '_id clerkId name picture',
        },
        {
          path: 'tags',
          model: Tag,
          select: '_id name',
        },
      ],
    });


    if (!tag) {
      throw new Error('Tag not found');
    }

    const questions = tag.questions;

    const isNext = questionsCount?.questions.length > page * pageSize;

    return { tagTitle: tag.name, questions, isNext };
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function getPopularTags() {
  try {
    connectToDatabase();

    const tags = await Tag.aggregate([
      { $project: { name: 1, count: { $size: '$questions' } } },
      { $sort: { count: -1 } },
      { $limit: 5 },
    ]);

    //* Another way to fetch popular tags :
    // const tags = await Tag.find({}).sort({ questions: -1 }).limit(5);

    return { tags };
  } catch (error) {
    console.log(error);
    throw error;
  }
}
