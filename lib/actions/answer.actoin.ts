'use server';
import Answer from '@/database/answer.model';
import Question from '@/database/question.model';
// import User from '@/database/user.model';
import Interactions from '@/database/interactions.model';
import User from '@/database/user.model';
import { revalidatePath } from 'next/cache';
import { connectToDatabase } from '../mongoose';
import {
  CreateAnswerParams,
  DeleteAnswerParams,
  GetAnswersParams,
} from './shared.types';

export const getAnswers = async (params: GetAnswersParams) => {
  try {
    connectToDatabase();

    const { questionId, sortBy } = params;

    let sortOptions = {};

    switch (sortBy) {
      case 'highestUpvotes':
        sortOptions = { upvotes: -1 };
        break;
      case 'lowestUpvotes':
        sortOptions = { upvotes: 1 };
        break;
      case 'recent':
        sortOptions = { createdAt: -1 };
        break;
      case 'old':
        sortOptions = { createdAt: 1 };
        break;
    }

    const answers = await Answer.find({ question: questionId })
      .populate('author', '_id clerkId name picture')
      .sort(sortOptions);

    return answers;
  } catch (error) {
    console.log(error);
  }
};

export const createAnswer = async (params: CreateAnswerParams) => {
  try {
    connectToDatabase();

    const { content, author, questionId, path } = params;

    const answer = await Answer.create({
      question: questionId,
      content,
      author,
    });

    const question = await Question.findByIdAndUpdate(questionId, {
      $push: { answers: answer._id },
    });

    // Create an interaction record for the user's ask_question action
    await Interactions.create({
      user: author,
      question: question._id,
      actions: 'answer',
      answer: answer._id,
      tags: question.tags,
    });

    // Increment author's reputation by +5 for creating a question
    await User.findByIdAndUpdate(author, { $inc: { reputation: 10 } });

    revalidatePath(path);
  } catch (error) {
    console.log(error);
  }
};

export const deleteAnswer = async ({ answerId, path }: DeleteAnswerParams) => {
  try {
    connectToDatabase();

    await Answer.findByIdAndDelete(answerId);
    await Question.updateOne(
      { answers: answerId },
      { $pull: { answers: answerId } }
    );

    revalidatePath(path);
  } catch (error) {
    throw new Error('Error deleting answer');
  }
};
