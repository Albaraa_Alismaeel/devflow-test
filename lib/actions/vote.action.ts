'use server';
import Answer from '@/database/answer.model';
import Question from '@/database/question.model';
import User from '@/database/user.model';
import { revalidatePath } from 'next/cache';
import { connectToDatabase } from '../mongoose';
import { AnswerVoteParams, QuestionVoteParams } from './shared.types';

export const questionVote = async (params: QuestionVoteParams) => {
  try {
    connectToDatabase();
    const { questionId, userId, hasupVoted, hasdownVoted, voteType, path } =
      params;

    const updateObject: any = {};
    let pullFrom, pushTo;

    if (voteType === 'up') {
      pullFrom = 'downvotes';
      pushTo = 'upvotes';
    } else {
      pullFrom = 'upvotes';
      pushTo = 'downvotes';
    }

    if (
      (hasupVoted && voteType === 'up') ||
      (hasdownVoted && voteType === 'down')
    ) {
      updateObject.$pull = { [pushTo]: userId };
      await User.findByIdAndUpdate(userId, { $inc: { reputation: -1 } });
    } else {
      updateObject.$pull = { [pullFrom]: userId };
      updateObject.$push = { [pushTo]: userId };
      await User.findByIdAndUpdate(userId, { $inc: { reputation: 1 } });
    }

    const question = await Question.findByIdAndUpdate(questionId, updateObject);

    await User.findByIdAndUpdate(question.author, {
      $inc: {
        reputation: hasupVoted
          ? -10
          : hasdownVoted
            ? 7
            : voteType === 'up'
              ? 10
              : -7,
      },
    });

    revalidatePath(path);
  } catch (error) {
    console.log(error);
  }
};

export const answerVote = async (params: AnswerVoteParams) => {
  try {
    connectToDatabase();
    const { answerId, userId, hasupVoted, hasdownVoted, voteType, path } =
      params;

    const updateObject: any = {};
    let pullFrom, pushTo;

    if (voteType === 'up') {
      pullFrom = 'downvotes';
      pushTo = 'upvotes';
    } else {
      pullFrom = 'upvotes';
      pushTo = 'downvotes';
    }

    if (
      (hasupVoted && voteType === 'up') ||
      (hasdownVoted && voteType === 'down')
    ) {
      updateObject.$pull = { [pushTo]: userId };
      await User.findByIdAndUpdate(userId, { $inc: { reputation: -1 } });
    } else {
      updateObject.$pull = { [pullFrom]: userId };
      updateObject.$push = { [pushTo]: userId };
      await User.findByIdAndUpdate(userId, { $inc: { reputation: 1 } });
    }
    const answer = await Answer.findByIdAndUpdate(answerId, updateObject);

    await User.findByIdAndUpdate(answer.author, {
      $inc: {
        reputation: hasupVoted
          ? -10
          : hasdownVoted
            ? 7
            : voteType === 'up'
              ? 10
              : -7,
      },
    });
    revalidatePath(path);
  } catch (error) {
    console.log(error);
  }
};
