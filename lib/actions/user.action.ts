'use server';

import Answer from '@/database/answer.model';
import Question from '@/database/question.model';
import Tag from '@/database/tag.model';
import User from '@/database/user.model';
import { BadgeCriteriaType } from '@/types';
import { FilterQuery } from 'mongoose';
import { revalidatePath } from 'next/cache';
import { connectToDatabase } from '../mongoose';
import { assignBadge } from '../utils';
import {
  CreateUserParams,
  DeleteUserParams,
  GetAllUsersParams,
  GetSavedQuestionsParams,
  GetUserByIdParams,
  GetUserStatsParams,
  ToggleSaveQuestionParams,
  UpdateUserParams,
} from './shared.types';

export async function getAllUsers(params: GetAllUsersParams) {
  try {
    connectToDatabase();
    const { searchQuery, filter, page = 1, pageSize = 15 } = params;

    const query: FilterQuery<typeof Question> = {};

    let sortOptions = {};
    switch (filter) {
      case 'new_users':
        sortOptions = { joinedAt: -1 };
        break;
      case 'old_users':
        sortOptions = { joinedAt: 1 };
        break;
      case 'top_contributors':
        sortOptions = { reputation: -1 };
        break;
    }

    if (searchQuery) {
      query.$or = [
        { name: { $regex: new RegExp(searchQuery, 'i') } },
        { username: { $regex: new RegExp(searchQuery, 'i') } },
      ];
    }

    const users = await User.find(query)
      .skip((page - 1) * pageSize)
      .limit(pageSize)
      .sort(sortOptions);

    const totalUsers = await User.countDocuments(query);

    const isNext = totalUsers > page * pageSize;

    return { users, isNext };
  } catch (error) {
    console.log(error);
    throw error;
  }
}
export async function getUserById(params: GetUserByIdParams) {
  try {
    connectToDatabase();

    const { userId } = params;

    const user = await User.findOne({ clerkId: userId });

    return user;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function createUser(userData: CreateUserParams) {
  try {
    connectToDatabase();

    const newUser = await User.create(userData);

    return newUser;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function updateUser(params: UpdateUserParams) {
  try {
    connectToDatabase();

    const { clerkId, updateData, path } = params;

    await User.findOneAndUpdate({ clerkId }, updateData, {
      new: true,
    });

    revalidatePath(path);
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function deleteUser(params: DeleteUserParams) {
  try {
    connectToDatabase();

    const { clerkId } = params;

    const user = await User.findOneAndDelete({ clerkId });

    if (!user) {
      throw new Error('User not found');
    }

    // Delete user from database
    // and questions, answers, comments, etc.

    // get user question ids
    // const userQuestionIds = await Question.find({ author: user._id}).distinct('_id');

    // delete user questions
    await Question.deleteMany({ author: user._id });

    // TODO: delete user answers, comments, etc.

    const deletedUser = await User.findByIdAndDelete(user._id);

    return deletedUser;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function toggleSaveQuestion(params: ToggleSaveQuestionParams) {
  try {
    connectToDatabase();

    const { userId, questionId, path } = params;

    console.log(userId, questionId, path);

    const user = await User.findById(userId);

    if (!user) {
      throw new Error('User not found');
    }

    const questionIndex = user.saved.indexOf(questionId);

    if (questionIndex === -1) {
      await User.findByIdAndUpdate(userId, {
        $addToSet: { saved: questionId },
      });
    } else {
      await User.findByIdAndUpdate(userId, {
        $pull: { saved: questionId },
      });
    }

    revalidatePath(path);
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function getSavedQuestions(params: GetSavedQuestionsParams) {
  try {
    connectToDatabase();
    const { clerkId, searchQuery, filter, page = 1, pageSize = 15 } = params;

    const query: FilterQuery<typeof User> = searchQuery
      ? { title: { $regex: new RegExp(searchQuery, 'i') } }
      : {};

    let sortOptions = {};
    switch (filter) {
      case 'most_recent':
        sortOptions = { createdAt: -1 };
        break;
      case 'oldest':
        sortOptions = { createdAt: 1 };
        break;
      case 'most_voted':
        sortOptions = { upvotes: -1 };
        break;
      case 'most_viewed':
        sortOptions = { views: -1 };
        break;
      case 'most_answered':
        sortOptions = { answers: -1 };
        break;
    }

    const user = await User.findOne({ clerkId });
    if (!user) {
      throw new Error('User not found');
    }

    const totalQuestionsCount = await Question.countDocuments({
      _id: { $in: user.saved },
      ...query,
    });

    const savedQuestions = await User.findOne({ clerkId }).populate({
      path: 'saved',
      model: Question,
      match: query,
      options: {
        sort: sortOptions,

        skip: (page - 1) * pageSize,
        limit: pageSize,
      },
      populate: [
        {
          path: 'author',
          model: User,
          select: '_id clerkId name picture',
        },
        {
          path: 'tags',
          model: Tag,
          select: '_id name',
        },
      ],
    });

    if (!user) {
      throw new Error('User not found');
    }

    const result = savedQuestions?.saved;
    const isNext = totalQuestionsCount > page * pageSize;

    return { savedQuestions: result, isNext };
  } catch (error) {
    console.log(error);
  }
}

export async function getUserInfo(params: GetUserByIdParams) {
  try {
    connectToDatabase();

    const { userId } = params;

    const user = await User.findOne({ clerkId: userId });

    if (!user) {
      throw new Error('User not found');
    }

    const totalQuestions = await Question.countDocuments({ author: user._id });
    const totalAnswer = await Answer.countDocuments({ author: user._id });

    const [questionsUpvotes] = await Question.aggregate([
      { $match: { author: user._id } },
      { $project: { upvotes: { $size: '$upvotes' }, _id: 0 } },
      { $group: { _id: null, totalUpvotes: { $sum: '$upvotes' } } },
    ]);
    const [answersUpvotes] = await Answer.aggregate([
      { $match: { author: user._id } },
      { $project: { upvotes: { $size: '$upvotes' }, _id: 0 } },
      { $group: { _id: null, totalUpvotes: { $sum: '$upvotes' } } },
    ]);
    const [questionsViews] = await Question.aggregate([
      { $match: { author: user._id } },
      { $group: { _id: null, totalViews: { $sum: '$views' } } },
    ]);

    const criteria = [
      { type: 'QUESTION_COUNT' as BadgeCriteriaType, count: totalQuestions },
      { type: 'ANSWER_COUNT' as BadgeCriteriaType, count: totalAnswer },
      {
        type: 'QUESTION_UPVOTES' as BadgeCriteriaType,
        count: questionsUpvotes?.totalUpvotes || 0,
      },
      {
        type: 'ANSWER_UPVOTES' as BadgeCriteriaType,
        count: answersUpvotes?.totalUpvotes || 0,
      },
      {
        type: 'TOTAL_VIEWS' as BadgeCriteriaType,
        count: questionsViews?.totalViews || 0,
      },
    ];

    const badgeCounts = assignBadge({ criteria })

    return {
      user,
      totalQuestions,
      totalAnswer,
      badges: badgeCounts,
    };
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export async function getUserQuestions({
  userId,
  page = 1,
  pageSize = 10,
}: GetUserStatsParams) {
  try {
    connectToDatabase();

    const questions = await Question.find({ author: userId })
      .populate({ path: 'tags', model: Tag })
      .populate({ path: 'author', model: User })
      .limit(pageSize)
      .skip((page - 1) * pageSize)
      .sort({ createdAt: -1, upvotes: -1, views: -1 });

    const totalQuestions = await Question.countDocuments({ author: userId });
    const isNext = totalQuestions > page * pageSize;

    return { questions, isNext };
  } catch (error) {
    console.log(error);
  }
}
export async function getuserAnswers({
  userId,
  page = 1,
  pageSize = 10,
}: GetUserStatsParams) {
  try {
    connectToDatabase();

    const answers = await Answer.find({ author: userId })
      .populate({ path: 'author', model: User })
      .populate('question', '_id title')
      .sort({ upvotes: -1, views: -1 })
      .limit(pageSize)
      .skip((page - 1) * pageSize);

    console.log();

    const totalAnswers = await Answer.countDocuments({ author: userId });
    const isNext = totalAnswers > page * pageSize;

    return { answers, isNext };
  } catch (error) {
    console.log(error);
  }
}
