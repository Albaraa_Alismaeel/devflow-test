import { z } from 'zod';

export const questionSchema = z.object({
  title: z.string().min(2).max(50),
  explanation: z.string().min(20),
  tags: z.array(z.string().min(3).max(15)).min(1).max(5),
});

export const answerSchema = z.object({
  answer: z.string().min(20),
});

export const profileSchema = z.object({
  name: z.string().min(2).max(50),
  username: z.string().min(3).max(15),
  portfolio: z.string().url().optional(),
  location: z.string().optional(),
  bio: z.string().max(1000).optional(),
});
