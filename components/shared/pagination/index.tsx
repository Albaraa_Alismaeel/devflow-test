'use client';

import { Button } from '@/components/ui/button';
import { formUrlQuery } from '@/lib/utils';
import { useRouter, useSearchParams } from 'next/navigation';

const Pagination = ({
  pageNumber,
  isNext,
}: {
  pageNumber: number;
  isNext: boolean;
}) => {
  const route = useRouter();
  const searchParams = useSearchParams();

  const handelNavigation = (direction: string) => {
    const page = direction === 'prev' ? pageNumber - 1 : pageNumber + 1;
    const newUrl = formUrlQuery({
      params: searchParams.toString(),
      key: 'page',
      value: page.toString(),
    });
    route.push(newUrl);
  };
  if (!isNext && pageNumber === 1) return null;

  return (
    <div className="mt-10 flex w-full items-center justify-center gap-2">
      <Button
        disabled={pageNumber === 1}
        onClick={() => handelNavigation('prev')}
        className="light-border-2 btn flex h-9 min-h-[36px] items-center justify-center gap-2 rounded-md border bg-slate-900 px-4 py-2 text-sm font-medium text-slate-50 shadow transition-colors hover:bg-slate-900/90 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-slate-950 disabled:pointer-events-none disabled:opacity-50 dark:bg-slate-50 dark:text-slate-900 dark:hover:bg-slate-50/90 dark:focus-visible:ring-slate-300"
      >
        <p className="body-medium text-dark200_light800">Prev</p>
      </Button>
      <div className="flex gap-2">
        <div className="flex items-center justify-center rounded-md bg-primary-500 px-3.5 py-2">
          <p className="body-semibold text-slate-50">{pageNumber}</p>
        </div>
      </div>
      <Button
        disabled={!isNext}
        onClick={() => handelNavigation('next')}
        className="light-border-2 btn flex h-9 min-h-[36px] items-center justify-center gap-2 rounded-md border bg-slate-900 px-4 py-2 text-sm font-medium text-slate-50 shadow transition-colors hover:bg-slate-900/90 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-slate-950 disabled:pointer-events-none disabled:opacity-50 dark:bg-slate-50 dark:text-slate-900 dark:hover:bg-slate-50/90 dark:focus-visible:ring-slate-300"
      >
        <p className="body-medium text-dark200_light800">Next</p>
      </Button>
    </div>
  );
};

export default Pagination;
