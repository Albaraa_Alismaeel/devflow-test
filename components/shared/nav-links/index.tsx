'use client';
import { sidebarLinks } from '@/constants';
import { useAuth } from '@clerk/nextjs';
import Image from 'next/image';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
type NavLinksProps = {
  className?: string;
  hide?: boolean;
};
const NavLinks = ({ className, hide }: NavLinksProps) => {
  const pathname = usePathname();
  const { userId } = useAuth();
  return (
    <section className={`flex grow flex-col gap-6  pb-4 ${className}`}>
      {sidebarLinks.map((link) => {
        const isActive =
          pathname === link.route ||
          (pathname.includes(link.route) && link.route.length > 1);
        if (link.route === '/profile') {
          if (userId) {
            link.route = `${link.route}/${userId}`;
          } else {
            return null;
          }
        }
        return (
          <Link
            key={link.route}
            href={link.route}
            className={`${isActive ? 'primary-gradient rounded-lg text-light-900' : 'text-dark300_light900'} flex items-center justify-start gap-4 bg-transparent p-4`}
          >
            <Image
              src={link.imgURL}
              width={20}
              height={20}
              alt={link.label}
              className={`${isActive ? '' : 'invert-colors'}`}
            />
            <p
              className={`${isActive ? 'base-bold' : 'base-medium'} ${hide && 'max-lg:hidden'}`}
            >
              {link.label}
            </p>
          </Link>
        );
      })}
    </section>
  );
};

export default NavLinks;
