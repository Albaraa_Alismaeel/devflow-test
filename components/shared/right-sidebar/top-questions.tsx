import { getHotQuestoins } from '@/lib/actions/question.action';
import Image from 'next/image';
import Link from 'next/link';

const TopQuestions = async () => {
  const {question} = await getHotQuestoins();
  return (
    <section className="flex flex-col gap-7 text-white">
      <h3 className="h3-bold text-dark200_light900 ">Top Questions</h3>
      <ul className="flex flex-col gap-[30px]">
        {question.map((question, i) => (
          <li key={i}>
            <Link href={`/question/${question._id}`}>
              <div className="flex items-center justify-between gap-7">
                <p className="body-medium text-dark500_light700">
                  {question.title}
                </p>
                <Image
                  src="/assets/icons/chevron-right.svg"
                  alt="chevron-right"
                  width={20}
                  height={20}
                  className='invert-colors'
                />
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default TopQuestions;
