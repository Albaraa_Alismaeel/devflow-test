import PopularTags from "./popular-tags";
import TopQuestions from "./top-questions";

const RightSidebar = () => {
  return (
    <aside className="custom-scrollbar background-light900_dark200 light-border sticky right-0 top-0 flex h-screen flex-col gap-20 overflow-auto border-s p-6 pt-[100px] shadow-light-300 dark:shadow-none max-xl:hidden  lg:pt-[120px] xl:w-[350px]">
      <TopQuestions/>
      <PopularTags/>
    </aside>
  )
}

export default RightSidebar;