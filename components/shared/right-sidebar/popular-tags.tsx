import TagBtn from '@/components/ui/tag-btn';
import { getPopularTags } from '@/lib/actions/tag.actions';
import Link from 'next/link';

const PopularTags = async () => {
  const { tags } = await getPopularTags();
  
  return (
    <section className="flex flex-col gap-7 ">
      <h3 className="h3-bold">Popular Tags</h3>
      <ul className="flex flex-col gap-[30px]">
        {tags.map((tag, idx) => (
          <li key={tag._id}>
            <Link href={`/tags/${tag._id}`}>
              <div className="flex items-center justify-between gap-7">
                <div>
                  <TagBtn title={tag.name} />
                </div>
                <p className="small-medium text-dark500_light700">
                  {tag.count}+
                </p>
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default PopularTags;
