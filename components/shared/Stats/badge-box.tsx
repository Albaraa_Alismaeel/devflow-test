import Image from 'next/image';
import React from 'react';

type Props = {
  imgUrl: string;
  title: string;
  value: number;
};

const BadgeBox = ({ imgUrl, title, value }: Props) => {
  return (
    <div
      className={`light-border background-light900_dark300 flex flex-wrap items-center  justify-start gap-4 rounded-md border p-6 shadow-light-300 dark:shadow-dark-200`}
    >
      <div>
        <Image src={imgUrl} alt="icon" width={40} height={50} />
      </div>
      <div>
        <h6 className="paragraph-semibold text-dark200_light900">{value}</h6>
        <p className="body-medium text-dark400_light700">{title}</p>
      </div>
    </div>
  );
};

export default BadgeBox;
