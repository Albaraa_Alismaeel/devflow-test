import BadgeBox from './badge-box';

type Props = {
  reputation: number;
  totalQuestions: number;
  totalAnswer: number;
  goldBadges: number;
  silverBadges: number;
  bronzeBadges: number;
};

const Stats = ({
  reputation,
  totalQuestions,
  totalAnswer,
  goldBadges,
  silverBadges,
  bronzeBadges,
}: Props) => {
  return (
    <div className="mt-10">
      <h4 className="h3-semibold text-dark200_light900">Stats - {reputation}</h4>
      <div className="mt-5 grid grid-cols-1 gap-5 xs:grid-cols-2 md:grid-cols-4">
        <div
          className={`light-border background-light900_dark300 flex flex-wrap items-center  justify-start gap-4 rounded-md border p-6 shadow-light-300 dark:shadow-dark-200`}
        >
          <div>
            <h6 className="paragraph-semibold text-dark200_light900">
              {totalQuestions}
            </h6>
            <p className="body-medium text-dark400_light700">Questions</p>
          </div>
          <div>
            <h6 className="paragraph-semibold text-dark200_light900">
              {totalAnswer}
            </h6>
            <p className="body-medium text-dark400_light700">Answers</p>
          </div>
        </div>
        <BadgeBox
          imgUrl="/assets/icons/gold-medal.svg"
          title="Gold Badges"
          value={goldBadges}
        />
        <BadgeBox
          imgUrl="/assets/icons/silver-medal.svg"
          title="Silver Badges"
          value={silverBadges}
        />
        <BadgeBox
          imgUrl="/assets/icons/bronze-medal.svg"
          title="Bronze Badges"
          value={bronzeBadges}
        />
      </div>
    </div>
  );
};

export default Stats;
