import QuestionCard from '@/components/cards/question';
import { getUserQuestions } from '@/lib/actions/user.action';
import { SearchParamsProps } from '@/types';
import NoResult from '../no-result';
import Pagination from '../pagination';

interface Props extends SearchParamsProps {
  userId: string;
}

const QuestionsTab = async ({ userId, searchParams }: Props) => {
  const { questions, isNext }: any = await getUserQuestions({
    userId,
    pageSize: 7,
    page: searchParams.page ? +searchParams.page : 1,
  });

  return (
    <div className="mt-10 flex w-full flex-col gap-6">
      {questions.length > 0 ? (
        <>
          {questions.map((question: any) => (
            <QuestionCard
              key={question.id}
              question={question}
              isEditable={true}
            />
          ))}
          <Pagination
            pageNumber={searchParams?.page ? +searchParams.page : 1}
            isNext={isNext}
          />
        </>
      ) : (
        <NoResult
          title="There's no questions to show"
          desc="Be the first to break the silence! 🚀 Ask a Question and kickstart the discussion. our query could be the next big thing others learn from. Get involved! 💡"
          withImg={true}
        />
      )}
    </div>
  );
};

export default QuestionsTab;
