import AnswerCard from '@/components/cards/answer';
import { getuserAnswers } from '@/lib/actions/user.action';
import { SearchParamsProps } from '@/types';
import NoResult from '../no-result';
import Pagination from '../pagination';

interface Props extends SearchParamsProps {
  userId: string;
}

const AnswersTab = async ({ userId, searchParams }: Props) => {
  const { answers, isNext }: any = await getuserAnswers({
    userId,
    pageSize: 7,
    page: searchParams.page ? +searchParams.page : 1,
  });

  return (
    <div className="mt-10 flex w-full flex-col gap-6">
      {answers.length > 0 ? (
        <>
          {answers.map((answer: any) => (
            <AnswerCard key={answer.id} answer={answer} />
          ))}

          <Pagination
            pageNumber={searchParams?.page ? +searchParams.page : 1}
            isNext={isNext}
          />
        </>
      ) : (
        <NoResult
          title="There's no questions to show"
          desc="Be the first to break the silence! 🚀 Ask a Question and kickstart the discussion. our query could be the next big thing others learn from. Get involved! 💡"
          withImg={true}
        />
      )}
    </div>
  );
};

export default AnswersTab;
