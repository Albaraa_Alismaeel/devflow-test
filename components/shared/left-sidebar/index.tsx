import { Button } from '@/components/ui/button';
import { SignOutButton, SignedIn, SignedOut } from '@clerk/nextjs';
import Image from 'next/image';
import Link from 'next/link';
import NavLinks from '../nav-links';

const LeftSidebar = () => {
  return (
    <aside className="custom-scrollbar background-light900_dark200 light-border sticky left-0 top-0 flex h-screen flex-col justify-between overflow-auto border-e p-6 pt-[100px] shadow-light-300 dark:shadow-none max-sm:hidden   lg:w-[266px] lg:pt-[120px]">
      <NavLinks hide={true} />
      <SignedOut>
        <div className="flex flex-col gap-3">
          <Link href="/sign-in">
            <Button className="small-medium btn-secondary min-h-[41px] w-full rounded-lg px-4 py-3 shadow-none">
              <span className="primary-text-gradient max-lg:hidden">Login</span>
              <Image
                src="/assets/icons/account.svg"
                width={20}
                height={20}
                alt="Login"
                className="invert-colors lg:hidden "
              />
            </Button>
          </Link>

          <Link href="/sign-up">
            <Button className="small-medium btn-tertiary light-border-2 min-h-[41px] w-full rounded-lg px-4 py-3 shadow-none">
              <span className="text-dark400_light900 max-lg:hidden">
                Sign up
              </span>
              <Image
                src="/assets/icons/sign-up.svg"
                width={20}
                height={20}
                alt="Signup"
                className="invert-colors lg:hidden "
              />
            </Button>
          </Link>
        </div>
      </SignedOut>
      <SignedIn>
        <SignOutButton>
          <Button className="flex items-center justify-start gap-4 bg-transparent p-4">
            <Image
              src="/assets/icons/sign-up.svg"
              width={20}
              height={20}
              alt="Signout"
              // className="lg:hidden"
            />
            <span className="text-dark400_light900 max-lg:hidden">
              Sign Out
            </span>
          </Button>
        </SignOutButton>
      </SignedIn>
    </aside>
  );
};

export default LeftSidebar;
