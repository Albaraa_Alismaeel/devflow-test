import Image from "next/image";
import Link from "next/link";

const Logo = () => {
  return (
    <Link href="/" className="flex items-center gap-1">
      <Image
        src="/assets/images/site-logo.svg"
        width={31}
        height={31}
        alt="DevFlow"
      />
      <p className="h2-bold font-spaceGrotesk text-dark-100 dark:text-light-900 max-sm:hidden">
        Dev <span className="text-primary-500">Overflow</span>
      </p>
    </Link>
  );
};

export default Logo;
