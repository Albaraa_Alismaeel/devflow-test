import { SignedIn, UserButton } from '@clerk/nextjs';
// import { ModeToggle } from '../../theme-toggler';
import { ModeToggle } from '@/components/theme-toggler';
import GlobalSearch from '../search/GlobalSearch';
import Logo from './logo';
import MobileNav from './mobileNav';

const Navbar = () => {
  return (
    <nav className="flex-between background-light900_dark200 fixed z-50 w-full gap-5 p-6 shadow-light-300 dark:shadow-none sm:px-12">
      <Logo />
      <GlobalSearch placeholder='Search anything globally...' hide={true} />
      <div className="flex-between gap-5">
        <ModeToggle />
        <SignedIn>
          <UserButton
            appearance={{
              elements: {
                avatarBox: 'h-10 w-10',
              },
              variables: {
                colorPrimary: '#ff7000',
              },
            }}
          />
        </SignedIn>
        <MobileNav />
      </div>
    </nav>
  );
};

export default Navbar;
