import Image from 'next/image';
import Link from 'next/link';
import { Button } from '../ui/button';

const NoResult = ({
  title,
  desc,
  withImg,
  action,
  route,
}: {
  title: string;
  desc: string;
  withImg?: boolean;
  action?: string;
  route?: string;
}) => {
  return (
    <div className="mt-10 flex h-[400px] flex-col items-center  justify-center text-center">
      {withImg && (
        <div>
          <Image
            src="/assets/images/dark-illustration.png"
            alt="No result found"
            width={269}
            height={200}
            className="absolute rotate-90 scale-0 transition-all duration-1000 dark:rotate-0 dark:scale-100"
          />
          <Image
            src="/assets/images/light-illustration.png"
            alt="No result found"
            width={269}
            height={200}
            className="rotate-0 scale-100 transition-all duration-1000 dark:rotate-90 dark:scale-0"
          />
        </div>
      )}
      <div className="mt-8">
        <h3 className="h2-bold text-dark200_light900">{title}</h3>
        <p className="body-regular text-dark500_light700 my-3.5 max-w-md text-center">
          {desc}
        </p>
      </div>
      {action && (
        <Link href={route || ''} className="mt-5 flex justify-center">
          <Button className="primary-gradient min-h-[46px] px-4 py-3 !text-light-900">
            {action}
          </Button>
        </Link>
      )}
    </div>
  );
};

export default NoResult;
