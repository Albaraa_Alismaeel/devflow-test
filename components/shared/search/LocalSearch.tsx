'use client';
import { Input } from '@/components/ui/input';
import { formUrlQuery, removeKeyesFromQuery } from '@/lib/utils';
import Image from 'next/image';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';

type props = {
  // route: string;
  iconPosition?: string;
  icon?: string;
  placeholder: string;
  classes?: string;
};
const LocalSearch = ({
  // route,
  iconPosition,
  icon,
  placeholder,
  classes,
}: props) => {
  const route = useRouter();
  const path = usePathname();
  const searchParams = useSearchParams();

  const query = searchParams.get('q');

  const [input, setinput] = useState(query || '');

  useEffect(() => {
    const time = setTimeout(() => {
      if (input) {
        const newUrl = formUrlQuery({
          params: searchParams.toString(),
          key: 'q',
          value: input,
        });
        route.push(newUrl, { scroll: false });
      } else {
        const newUrl = removeKeyesFromQuery({
          params: searchParams.toString(),
          keys: ['q'],
        });
        route.push(newUrl, { scroll: false });
      }
    }, 300);

    return () => clearTimeout(time);
  }, [input, path, route, searchParams]);

  return (
    <div
      className={`background-light800_darkgradient flex min-h-[56px] grow items-center gap-4 rounded-[10px] px-4 ${classes}`}
    >
      {(!iconPosition || iconPosition === 'left') && (
        <Image
          src={icon || 'assets/icons/search.svg'}
          alt="search icon"
          width={24}
          height={24}
          className="cursor-pointer "
        />
      )}
      <Input
        type="text"
        placeholder={placeholder}
        value={input}
        onChange={(e) => {
          setinput(e.target.value);
        }}
        className="paragraph-regular no-focus placeholder text-dark400_light700 border-none bg-transparent shadow-none outline-none"
      />
      {iconPosition === 'right' && (
        <Image
          src={icon || 'assets/icons/search.svg'}
          alt="search icon"
          width={24}
          height={24}
          className="cursor-pointer "
        />
      )}
    </div>
  );
};

export default LocalSearch;
