'use client';
import { Input } from '@/components/ui/input';
import { formUrlQuery, removeKeyesFromQuery } from '@/lib/utils';
import Image from 'next/image';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import GlobalResult from './global-result';
type props = {
  placeholder: string;
  hide?: boolean;
};
const GlobalSearch = ({ placeholder, hide }: props) => {
  const route = useRouter();
  const path = usePathname();
  const searchParams = useSearchParams();

  const query = searchParams.get('gq');

  const [search, setSearch] = useState(query || '');
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (search) {
        const newUrl = formUrlQuery({
          params: searchParams.toString(),
          key: 'gq',
          value: search,
        });
        route.push(newUrl, { scroll: false });
      } else {
        const newUrl = removeKeyesFromQuery({
          params: searchParams.toString(),
          keys: ['gq', 'type'],
        });
        route.push(newUrl, { scroll: false });
      }
    }, 300);
    return () => clearTimeout(timer);
  }, [search, path, route, searchParams]);

  const handleChange = (val: string) => {
    setSearch(val);
    if (!isOpen) setIsOpen(true);
    if (isOpen && val === '') setIsOpen(false);
  };

  return (
    <>
      <div className={`relative w-full max-w-[600px] max-lg:hidden`}>
        <div className="text-dark400_light700 relative flex min-h-[56px] grow items-center gap-1 rounded-xl bg-transparent px-4">
          <Image
            src="/assets/icons/search.svg"
            alt="search"
            width={24}
            height={24}
            className="cursor-pointer"
          />
          <Input
            type="text"
            placeholder={placeholder}
            onChange={(e) => handleChange(e.target.value)}
            value={search}
            className="paragraph-regular no-focus placeholder background-light800_darkgradient border-none shadow-none outline-none"
          />
        </div>
        {isOpen && (
          <GlobalResult
            closeModal={() => {
              setIsOpen(false);
              setSearch('');
            }}
          />
        )}
      </div>
    </>
  );
};

export default GlobalSearch;
