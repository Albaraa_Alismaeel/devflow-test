import Image from 'next/image';
import Link from 'next/link';

const ResultLink = ({
  href,
  title,
  type,
  closeModal,
}: {
  href: string;
  title: string;
  type: string;
  closeModal: any
}) => {
  const link = (type: string, id: string)=> {
    switch (type) {
      case 'user':
        return `/profile/${id}`;
      case 'tag':
        return `/tags/${id}`;
      case 'question' || 'answer':
        return `/question/${id}`;
      default:
        return `/question/${id}`;
    }
  }

  return (
    <Link
      className="flex w-full cursor-pointer items-start gap-3 px-5 py-2.5 hover:bg-light-700/50 dark:hover:bg-dark-500/50"
      href={link(type, href)}
      onClick={closeModal}
    >
      <Image
        src="assets/icons/tag.svg"
        width={18}
        height={18}
        alt="tag"
        className="invert-colors mt-1 object-contain"
      />
      <div className="flex flex-col">
        <p className="body-medium text-dark200_light800 line-clamp-1">
          {title}
        </p>
        <p className="text-light400_light500 small-medium mt-1 font-bold capitalize">
          {type}
        </p>
      </div>
    </Link>
  );
};

export default ResultLink;
