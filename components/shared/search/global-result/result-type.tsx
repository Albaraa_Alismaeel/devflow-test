'use client';
import { GlobalSearchFilters } from '@/constants/filters';
import { formUrlQuery, removeKeyesFromQuery } from '@/lib/utils';
import { useRouter, useSearchParams } from 'next/navigation';
import { useState } from 'react';
const ResultType = () => {
  const route = useRouter();
  const searchParams = useSearchParams();
  const query = searchParams.get('type');

  const [active, setActive] = useState(query || '');

  const handleClick = (val: string) => {
    if (active === val) {
      setActive('');
      const newUrl = removeKeyesFromQuery({
        params: searchParams.toString(),
        keys: ['type'],
      });
      route.push(newUrl, { scroll: false });
    } else {
      setActive(val);
      const newUrl = formUrlQuery({
        params: searchParams.toString(),
        key: 'type',
        value: val.toLowerCase(),
      });
      route.push(newUrl, { scroll: false });
    }
  };

  return (
    <div className="flex items-center gap-5 px-5">
      <p className="text-dark400_light900 body-medium">Type:</p>
      <div className="flex gap-3">
        {GlobalSearchFilters.map((item) => (
          <button
            key={item.value}
            type="button"
            className={`light-border-2 small-medium rounded-2xl px-5 py-2 capitalize dark:text-light-800  ${active === item.value ? 'bg-primary-500 text-light-900' : 'bg-light-700 text-dark-400 hover:text-primary-500  dark:bg-dark-500 dark:hover:text-primary-500 '}`}
            onClick={() => handleClick(item.value)}
          >
            {item.name}
          </button>
        ))}
      </div>
    </div>
  );
};

export default ResultType;
