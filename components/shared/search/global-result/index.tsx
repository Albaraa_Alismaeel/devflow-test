'use client';
import { globalSearch } from '@/lib/actions/general.action';
import { ReloadIcon } from '@radix-ui/react-icons';
import { useSearchParams } from 'next/navigation';
import { useEffect, useRef, useState } from 'react';
import ResultLink from './result-link';
import ResultType from './result-type';

const GlobalResult = ({ closeModal }: { closeModal: any }) => {
  const searchParam = useSearchParams();
  const [isLoading, setIsLoading] = useState(false);
  const resultContainerRef = useRef(null);

  const [result, setResult] = useState([]);

  const global = searchParam.get('gq');
  const type = searchParam.get('type');

  useEffect(() => {
    const handleOutsideClick = (e: MouseEvent) => {
      if (
        resultContainerRef.current &&
        // @ts-ignore
        !resultContainerRef.current.contains(e.target)
      ) {
        closeModal();
      }
    };

    document.addEventListener('click', handleOutsideClick);
    return () => document.removeEventListener('click', handleOutsideClick);
  }, [closeModal, resultContainerRef]);

  useEffect(() => {
    const fetchResult = async () => {
      setResult([]);
      setIsLoading(true);
      try {
        const res = await globalSearch({
          query: global,
          type,
        });
        setResult(JSON.parse(res));
      } catch (error) {
        throw new Error('Error fetching data');
      } finally {
        setIsLoading(false);
      }
    };
    if (global) fetchResult();
  }, [global, type]);

  return (
    <div
      className="absolute top-full z-10 mt-3 w-full rounded-xl bg-light-800 py-5 shadow-sm dark:bg-dark-400"
      ref={resultContainerRef}
    >
      <ResultType />
      <div className="my-5 h-px bg-light-700/50 dark:bg-dark-500/50" />
      <div className="max-h-[55vh] space-y-5 overflow-auto">
        <p className="text-dark400_light900 paragraph-semibold px-5">
          Top Match
        </p>

        {isLoading ? (
          <div className="flex-center flex-col px-5">
            <ReloadIcon className="my-2 size-10 animate-spin text-primary-500" />
            <p className="text-dark200_light800 body-regular">
              Browsing the eniter database
            </p>
          </div>
        ) : (
          <div className="flex flex-col gap-2">
            {result.length > 0 ? (
              result.map((res: any) => (
                <ResultLink
                  key={res?.id}
                  href={res?.id}
                  title={res?.title}
                  type={res?.type}
                  closeModal={closeModal}
                />
              ))
            ) : (
              <div className="flex-center flex-col px-5">
                <p className="text-dark200_light800 body-regular px-5 py-2.5">
                  Oops, no results found
                </p>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default GlobalResult;
