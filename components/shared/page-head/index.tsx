import { Button } from '@/components/ui/button';
import Link from 'next/link';
import Filter from '../filter';
import LocalSearch from '../search/LocalSearch';

type PageHeadProps = {
  title: string;
  filters?: {
    name: string;
    value: string;
  }[];
  // route: string;
  placeholder: string;
  askButton?: boolean;
  withFilter?: boolean;
};
const PageHead = ({
  title,
  filters = [],
  // route,
  placeholder,
  askButton,
  withFilter = true,
}: PageHeadProps) => {


  
  return (
    <div>
      <div className="flex w-full flex-row items-center justify-between gap-4">
        <h1 className="h1-bold text-dark100_light900">{title}</h1>
        <Link href="/ask-question" className="flex justify-end">
          {askButton && (
            <Button className="primary-gradient min-h-[46px] px-4 py-3 !text-light-900">
              Ask a Question
            </Button>
          )}
        </Link>
      </div>
      <div className="mt-11 flex justify-between gap-5 max-sm:flex-col sm:items-center">
        <LocalSearch placeholder={placeholder} />
        {askButton ? (
          <Filter
            filters={filters}
            classes="min-h-[56px] sm:min-w-[170px]"
            containerClasses="md:hidden"
          />
        ) : (
          withFilter && (
            <Filter filters={filters} classes="min-h-[56px] sm:min-w-[170px]" />
          )
        )}
      </div>
    </div>
  );
};

export default PageHead;
