import Link from 'next/link';
import ParesHTML from '../shared/parse-html';
import { Avatar, AvatarImage } from '../ui/avatar';
import Votes from './Votes';

type Props = {
  _id: string;
  name: string;
  clerkId: string;
  picture: string;
  content: string;
  upvotes: {}[];
  downvotes: {}[];
  createdAt: string;
  userId: string;
};

const AnswerBox = ({
  _id,
  name,
  clerkId,
  picture,
  content,
  upvotes,
  downvotes,
  createdAt,
  userId,
}: Props) => {
  return (
    <div className="light-border border-b py-10">
      <div className="mb-8 flex flex-col-reverse justify-between gap-5 sm:flex-row sm:items-center sm:gap-2">
        <div className="flex flex-col gap-1 sm:flex-row sm:items-center">
          <Link
            href={`/profile/${clerkId}`}
            className="flex items-center justify-start gap-1"
          >
            <Avatar className="size-5">
              <AvatarImage src={picture}></AvatarImage>
            </Avatar>
            <p className="body-semibold text-dark300_light700">{name}</p>
          </Link>
          <p className="small-regular text-light400_light500 mt-0.5 line-clamp-1">
            <span className="max-sm:hidden"> • </span>
            <span>Answered {createdAt}</span>
          </p>
        </div>
        <div className="flex justify-end">
          <Votes
            type="answer"
            itemId={JSON.stringify(_id)}
            userId={JSON.stringify(userId)}
            upvotes={upvotes.length}
            downvotes={downvotes.length}
            upvoted={upvotes.includes(userId)}
            downvoted={downvotes.includes(userId)}
          />
        </div>
      </div>

      <div className="max-h-60 overflow-auto">
        <ParesHTML data={content} />
      </div>
    </div>
  );
};

export default AnswerBox;
