'use client';
import { toggleSaveQuestion } from '@/lib/actions/user.action';
import { answerVote, questionVote } from '@/lib/actions/vote.action';
import { formatBigNumber } from '@/lib/utils';
import Image from 'next/image';
import { usePathname } from 'next/navigation';

type Props = {
  type: string;
  itemId: string;
  userId: string;
  upvotes: number;
  downvotes: number;
  upvoted: boolean;
  downvoted: boolean;
  started?: boolean;
};

const Votes = ({
  type,
  itemId,
  userId,
  upvotes,
  downvotes,
  upvoted,
  downvoted,
  started,
}: Props) => {
  const pathname = usePathname();
  // const router = useRouter();
  const handleVote = async (action: 'up' | 'down') => {
    if (type === 'question') {
      await questionVote({
        questionId: JSON.parse(itemId),
        userId: JSON.parse(userId),
        hasupVoted: upvoted,
        hasdownVoted: downvoted,
        voteType: action,
        path: pathname,
      });
    } else {
      await answerVote({
        answerId: JSON.parse(itemId),
        userId: JSON.parse(userId),
        hasupVoted: upvoted,
        hasdownVoted: downvoted,
        voteType: action,
        path: pathname,
      });
    }
  };

  const handleSave = async () => {
    await toggleSaveQuestion({
      questionId: JSON.parse(itemId),
      userId: JSON.parse(userId),
      path: pathname,
    })
  };

  // useEffect(() => {
  //   viewQuestion({
  //     questionId: JSON.parse(itemId),
  //     userId: userId ? JSON.parse(userId) : null,
  //   })
  // }, [itemId, userId, pathname, router])

  return (
    <div className="flex gap-5">
      <div className="flex-center gap-2.5">
        <div className="flex-center gap-1.5">
          <Image
            src={
              upvoted ? '/assets/icons/upvoted.svg' : '/assets/icons/upvote.svg'
            }
            className="cursor-pointer"
            alt="upvote"
            width={18}
            height={18}
            onClick={() => handleVote('up')}
          />
          <div className="flex-center background-light700_dark400 min-w-[18px] rounded-sm p-1">
            <p className="subtle-medium text-dark400_light900">
              {formatBigNumber(upvotes)}
            </p>
          </div>
        </div>
        <div className="flex-center gap-1.5">
          <Image
            src={
              downvoted
                ? '/assets/icons/downvoted.svg'
                : '/assets/icons/downvote.svg'
            }
            className="cursor-pointer"
            alt="upvote"
            width={18}
            height={18}
            onClick={() => handleVote('down')}
          />
          <div className="flex-center background-light700_dark400 min-w-[18px] rounded-sm p-1">
            <p className="subtle-medium text-dark400_light900">
              {formatBigNumber(downvotes)}
            </p>
          </div>
        </div>
      </div>
      {type === 'question' && (
        <Image
          src={
            started
              ? '/assets/icons/star-filled.svg'
              : '/assets/icons/star-red.svg'
          }
          alt="star"
          width={18}
          height={18}
          className="cursor-pointer"
          onClick={() => handleSave()}
        />
      )}
    </div>
  );
};

export default Votes;
