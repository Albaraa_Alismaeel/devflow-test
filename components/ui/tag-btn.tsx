import { Badge } from './badge';

const TagBtn = ({ title }: { title: string }) => {
  return (
    <Badge className="subtle-medium background-light800_dark300 text-light400_light500 w-full justify-center rounded-md border-none px-4 py-2 uppercase">
      <span className='truncate'>{title}</span>
    </Badge>
  );
};

export default TagBtn;