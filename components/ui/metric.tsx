import Image from 'next/image';
import Link from 'next/link';
import { Avatar, AvatarFallback, AvatarImage } from './avatar';

interface MetricProps {
  imgUrl: string;
  alt: string;
  value: string | number;
  valueStyle?: string;
  title: string;
  isAvatar?: boolean;
  href?: string;
}

const Metric = ({
  imgUrl,
  alt,
  value,
  valueStyle,
  title,
  isAvatar,
  href,
}: MetricProps) => {
  const metricContent = (
    <>
      {isAvatar ? (
        <Avatar className="size-6">
          <AvatarImage src={imgUrl} />
          <AvatarFallback>{value}</AvatarFallback>
        </Avatar>
      ) : (
        <Image src={imgUrl} alt={alt} width={16} height={16} />
      )}
      {isAvatar ? (
        <h6 className="body-medium">{title}</h6>
      ) : (
        <p className="small-regular">
          {value} {title}
        </p>
      )}
    </>
  );

  if (href)
    return (
      <Link href={href} className="flex-center flex-wrap gap-1">
        {metricContent}
      </Link>
    );
    
  return <div className="flex-center flex-wrap gap-1">{metricContent}</div>;
};

export default Metric;
