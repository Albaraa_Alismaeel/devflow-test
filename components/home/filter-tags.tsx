'use client';
import { Button } from '@/components/ui/button';
import { HomePageFilters } from '@/constants/filters';
import { formUrlQuery, removeKeyesFromQuery } from '@/lib/utils';
import { useRouter, useSearchParams } from 'next/navigation';
import { useState } from 'react';

const FilterTags = () => {
  const route = useRouter();
  const searchParams = useSearchParams();
  const query = searchParams.get('f');

  const [active, setActive] = useState(query || '');

  const handleClick = (val: string) => {
    if (active === val) {
      setActive('');
      const newUrl = removeKeyesFromQuery({
        params: searchParams.toString(),
        keys: ['f'],
      });
      route.push(newUrl, { scroll: false });
    } else {
      setActive(val);
      const newUrl = formUrlQuery({
        params: searchParams.toString(),
        key: 'f',
        value: val.toLowerCase(),
      });
      route.push(newUrl, { scroll: false });
    }
  };
  return (
    <section className="mt-10 flex flex-wrap gap-3 max-md:hidden">
      {HomePageFilters.map((item) => (
        <Button
          className={`body-medium rounded-lg px-6 py-3 capitalize shadow-none 
          ${active === item.value 
            ? 'bg-primary-100 text-primary-500 hover:bg-primary-100 dark:bg-dark-400 dark:text-primary-500 dark:hover:bg-dark-400' 
            : 'bg-light-800 text-light-500 hover:bg-light-800 dark:bg-dark-300 dark:text-light-500 dark:hover:bg-dark-300'}`}
          key={item.value}
          onClick={() => handleClick(item.value)}
        >
          {item.name}
        </Button>
      ))}
    </section>
  );
};

export default FilterTags;
