'use client';

import { Button } from '@/components/ui/button';
import { themes } from '@/constants';
import { useTheme } from 'next-themes';
import Image from 'next/image';

export function ModeToggle() {
  const { setTheme, theme } = useTheme();

  const nextTheme = theme === 'light' ? 1 : theme === 'dark' ? 2 : 0

  const toggleTheme = () => {
    setTheme(themes[nextTheme].value)
  }

  return (
    <>
     {/* {nextTheme } */}
      <Button
        variant="ghost"
        className="shadow-none"
        size="icon"
        onClick={toggleTheme}
      >
        <Image
          src={themes[nextTheme].icon}
          alt={themes[nextTheme].label}
          width={20}
          height={20}
          className="active-theme"
        />
        <span className="sr-only text-black dark:text-white">Toggle theme</span>
      </Button>
    </>
  );
}