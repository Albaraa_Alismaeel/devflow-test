import { formatBigNumber, getTimestamp } from '@/lib/utils';
import { auth } from '@clerk/nextjs/server';
import Link from 'next/link';
import Metric from '../ui/metric';
import CardActions from './card-actions';
type props = {
  id: string;
  author: { clerkId: string; name: string; picture: string };
  upvotes: Array<object>;
  question: { _id: string; title: string };
  createdAt: Date;
};
const AnswerCard = ({
  answer,
  isSaved,
}: {
  answer: props;
  isSaved?: boolean;
}) => {
  const { userId } = auth();

  return (
    <div className="card-wrapper rounded-[10px] p-9 sm:px-11">
      <div className="mb-7">
        <span className="subtle-regular text-dark400_light700 line-clamp-1 flex sm:hidden">
          {getTimestamp(answer.createdAt)}
        </span>
        <div className="flex items-start justify-end">
          <Link
            href={`/question/${answer.question._id}#${answer.id}`}
            className={`grow ${isSaved ? 'me-8' : ''}`}
          >
            <h3 className="sm:h3-semibold base-semibold text-dark200_light900 mb-3.5 line-clamp-2">
              {answer.question.title}
            </h3>
          </Link>
          {userId === answer.author.clerkId && (
            <CardActions
              id={answer.id}
              isAnswer={true}
            />
          )}
        </div>
      </div>
      <div className="flex flex-wrap items-center justify-between gap-3">
        <div className="flex items-center gap-1.5">
          <Metric
            imgUrl={answer.author.picture}
            alt={answer.author.name}
            isAvatar={true}
            value={answer.author.name[0] + answer.author.name[1]}
            href={`/profile/${answer.author.clerkId}`}
            title={answer.author.name}
          />
          <p className="max-sm:hidden">•</p>
          <p className="small-regular max-sm:hidden">
            {getTimestamp(answer.createdAt)}
          </p>
        </div>
        <div className="flex items-center gap-3 max-sm:flex-wrap">
          <Metric
            imgUrl="/assets/icons/like.svg"
            alt="like"
            value={formatBigNumber(answer.upvotes.length)}
            title="Votes"
          />
        </div>
      </div>
    </div>
  );
};

export default AnswerCard;
