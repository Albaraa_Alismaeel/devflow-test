'use client';
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '@/components/ui/dialog';
import { useToast } from '@/components/ui/use-toast';
import { deleteAnswer } from '@/lib/actions/answer.actoin';
import { deleteQuestion } from '@/lib/actions/question.action';
import { toggleSaveQuestion } from '@/lib/actions/user.action';
import { DialogClose } from '@radix-ui/react-dialog';
import Image from 'next/image';
import { usePathname, useRouter } from 'next/navigation';
import { Button } from '../ui/button';
type props = {
  id: string;
  isQuestion?: boolean;
  isAnswer?: boolean;
  isSaved?: boolean;
  userId?: string;
};
const CardActions = ({
  id,
  isQuestion,
  isAnswer,
  isSaved,
  userId = '',
}: props) => {
  const { toast } = useToast();
  const pathname = usePathname();
  const route = useRouter();

  const handleSave = async () => {
    await toggleSaveQuestion({
      questionId: JSON.parse(id),
      userId: JSON.parse(userId),
      path: pathname,
    });
  };

  const handleEdit = () => {
    route.push(`/question/edit/${JSON.parse(id)}`);
  };

  const hadnleDelete = async () => {
    if (isQuestion) {
      await deleteQuestion({ questionId: JSON.parse(id), path: pathname });
      toast({
        title: 'Question deleted successfully!',
      });
    }
    if (isAnswer) {
      await deleteAnswer({ answerId: id, path: pathname });
      toast({
        title: 'Answer deleted successfully!',
      });
    }
  };

  return (
    <>
      <div className="flex gap-3">
        <Dialog>
          {isSaved ? (
            <Image
              src="/assets/icons/star-filled.svg"
              className="cursor-pointer"
              alt="star"
              width={24}
              height={24}
              onClick={() => handleSave()}
            />
          ) : (
            <>
              {isQuestion && (
                <Image
                  src="/assets/icons/edit.svg"
                  className="cursor-pointer"
                  alt="edit"
                  width={14}
                  height={14}
                  onClick={() => handleEdit()}
                />
              )}
              <DialogTrigger>
                <Image
                  src="/assets/icons/trash.svg"
                  className="cursor-pointer"
                  alt="trash"
                  width={14}
                  height={14}
                />
              </DialogTrigger>
            </>
          )}
          <DialogContent className="background-light900_dark300 border-none">
            <DialogHeader>
              <DialogTitle>Are you absolutely sure?</DialogTitle>
              <DialogDescription>
                This action cannot be undone. This will permanently delete your
                from our servers.
              </DialogDescription>
              <div className="mt-7 flex justify-end gap-3">
                <DialogClose asChild>
                  <Button type="button" className="bg-green-600 text-white">
                    Close
                  </Button>
                </DialogClose>
                <DialogClose asChild>
                  <Button
                    type="button"
                    className="bg-red-600 text-white"
                    onClick={() => hadnleDelete()}
                  >
                    I&apos;m sure
                  </Button>
                </DialogClose>
              </div>
            </DialogHeader>
          </DialogContent>
        </Dialog>
      </div>
    </>
  );
};

export default CardActions;
