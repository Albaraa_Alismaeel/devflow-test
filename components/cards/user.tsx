import { getTopInteractedTags } from '@/lib/actions/tag.actions';
import Link from 'next/link';
import { Avatar, AvatarFallback, AvatarImage } from '../ui/avatar';
import TagBtn from '../ui/tag-btn';
type UserCardProps = {
  _id: string;
  clerkId: string;
  name: string;
  picture: string;
  avatarFallback: string;
  username: string;
};

const UserCard = async ({
  _id,
  clerkId,
  name,
  picture,
  avatarFallback,
  username,
}: UserCardProps) => {
  const tags = await getTopInteractedTags({ userId: _id });
  return (
    <div className="background-light900_dark200 light-border flex w-full flex-col items-center justify-center rounded-2xl border p-8 ">
      <Link href={`/profile/${clerkId}`}>
        <div>
          <Avatar className="mx-auto h-[100p] w-[100px]">
            <AvatarImage src={picture} alt={name} />
            <AvatarFallback>{avatarFallback}</AvatarFallback>
          </Avatar>
        </div>
        <div className="mt-5 text-center">
          <h3
            className="h3-bold text-dark200_light900 line-clamp-1"
            title={name}
          >
            {name}
          </h3>
          <p className="body-regular text-dark500_light500 mt-2">
            <span>@</span>
            <span>{username}</span>
          </p>
        </div>
      </Link>
      <div className="mt-5">
        {tags.length > 0 ? (
          <ul className="grid grid-cols-3 items-center gap-2">
            {tags.map((tag) => (
              <li key={tag}>
                <Link href={`/tags/${tag}`}>
                  <TagBtn title={tag} />
                </Link>
              </li>
            ))}
          </ul>
        ) : (
          <TagBtn title="No Tags Yet." />
        )}
      </div>
    </div>
  );
};

export default UserCard;
