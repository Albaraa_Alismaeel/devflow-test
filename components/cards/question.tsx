import Link from 'next/link';
import TagBtn from '../ui/tag-btn';

import { formatBigNumber, getTimestamp } from '@/lib/utils';
import { auth } from '@clerk/nextjs/server';
import Metric from '../ui/metric';
import CardActions from './card-actions';
type props = {
  id: string;
  title: string;
  tags: { _id: string; name: string }[];
  author: { _id: string; clerkId: string; name: string; picture: string };
  upvotes: Array<object>;
  views: number;
  answers: Array<object>;
  createdAt: Date;
};
const QuestionCard = ({
  question,
  isEditable,
  isSaved,
}: {
  question: props;
  isEditable?: boolean;
  isSaved?: boolean;
}) => {
  const formatedValues = {
    upvotes: formatBigNumber(question.upvotes.length),
    views: formatBigNumber(question.views),
    answers: formatBigNumber(question.answers.length),
  };
  const { userId } = auth();
  return (
    <div className="card-wrapper rounded-[10px] p-9 sm:px-11">
      <div className="mb-7">
        <span className="subtle-regular text-dark400_light700 line-clamp-1 flex sm:hidden">
          {getTimestamp(question.createdAt)}
        </span>
        <div className="flex items-start justify-end">
          <Link
            href={`/question/${question.id}`}
            className={`grow ${isSaved ? 'me-8' : ''}`}
          >
            <h3 className="sm:h3-semibold base-semibold text-dark200_light900 mb-3.5 line-clamp-2">
              {question.title}
            </h3>
          </Link>
          {((userId === question.author.clerkId && isEditable) || isSaved) && (
            <CardActions
              id={JSON.stringify(question.id)}
              isQuestion={true}
              isSaved={isSaved}
              userId={JSON.stringify(question.author._id)}
            />
          )}
        </div>
        <ul className="flex flex-wrap gap-2">
          {question.tags.map((tag) => (
            <li key={tag._id}>
              <Link href={`/tags/${tag._id}`}>
                <TagBtn title={tag.name} />
              </Link>
            </li>
          ))}
        </ul>
      </div>
      <div className="flex flex-wrap items-center justify-between gap-3">
        <div className="flex items-center gap-1.5">
          <Metric
            imgUrl={question.author.picture}
            alt={question.author.name}
            isAvatar={true}
            value={question.author.name[0] + question.author.name[1]}
            href={`/profile/${question.author.clerkId}`}
            title={question.author.name}
          />
          <p className="max-sm:hidden">•</p>
          <p className="small-regular max-sm:hidden">
            asked {getTimestamp(question.createdAt)}
          </p>
        </div>
        <div className="flex items-center gap-3 max-sm:flex-wrap">
          <Metric
            imgUrl="/assets/icons/like.svg"
            alt="like"
            value={formatedValues.upvotes}
            title="Votes"
          />
          <Metric
            imgUrl="/assets/icons/message.svg"
            alt="comment"
            value={formatedValues.answers}
            title="Answers"
          />
          <Metric
            imgUrl="/assets/icons/eye.svg"
            alt="eye"
            value={formatedValues.views}
            title="Views"
          />
        </div>
      </div>
    </div>
  );
};

export default QuestionCard;
