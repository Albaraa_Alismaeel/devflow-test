import Link from "next/link";

interface TagCardProps {
  id: string;
  tag: string;
  questions: number;
}

const TagCard = ({id, tag, questions }: TagCardProps) => {
  return (
    <Link href={`/tags/${id}`} className="background-light900_dark200 light-border w-full rounded-2xl border px-8 py-10">
      <div className="background-light800_dark400 w-fit rounded-sm px-5 py-1.5">
        <p className="paragraph-semibold text-dark300_light900">{tag}</p>
      </div>
      <p className="small-regular text-dark500_light700 mt-4 line-clamp-5">
        JavaScript, often abbreviated as JS, is a programming language that is
        one of the core technologies of the World Wide Web, alongside HTML and
        CSS
      </p>
      <p className="small-medium text-dark400_light500 mt-3.5">
        <span className="body-semibold primary-text-gradient mr-2.5">
          {questions}+
        </span>
        <span>Questions</span>
      </p>
    </Link>
  );
};

export default TagCard;
