'use client';
import { createAnswer } from '@/lib/actions/answer.actoin';
import { answerSchema } from '@/lib/validation';
import { zodResolver } from '@hookform/resolvers/zod';
import { Editor } from '@tinymce/tinymce-react';
import { useTheme } from 'next-themes';
import Image from 'next/image';
import { usePathname } from 'next/navigation';
import { useRef } from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { Button } from '../ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from '../ui/form';

const Answer = ({
  userId,
  questionId,
}: {
  userId: string;
  questionId: string;
}) => {
  const { theme } = useTheme();
  const pathname = usePathname();
  const editorRef = useRef(null);
  const form = useForm<z.infer<typeof answerSchema>>({
    resolver: zodResolver(answerSchema),
    defaultValues: {
      answer: '',
    },
  });

  async function onSubmit(values: z.infer<typeof answerSchema>) {
    try {
      await createAnswer({
        content: values.answer,
        author: JSON.parse(userId),
        questionId: JSON.parse(questionId),
        path: pathname,
      });

      form.reset();
      if (editorRef.current) {
        const editor = editorRef.current as any;
        editor.setContent('');
      }
    } catch (e) {
      throw Error('Failed to post answer');
    }
  }

  return (
    <div>
      <div className="flex flex-col justify-between gap-5 sm:flex-row sm:items-center sm:gap-2">
        <div className="paragraph-semibold text-dark400_light800">
          Write Your Answer Here
        </div>
        <Button className="btn light-border-2 gap-1.5 rounded-md px-4 py-2.5 text-primary-500 shadow-none dark:text-primary-500">
          <Image
            src="/assets/icons/stars.svg"
            alt="stars"
            width={18}
            height={18}
          />
          <span>Generate AI answer</span>
        </Button>
      </div>
      <Form {...form}>
        <form
          className="mt-6 flex w-full flex-col gap-10"
          onSubmit={form.handleSubmit(onSubmit)}
        >
          <FormField
            control={form.control}
            name="answer"
            render={({ field }) => (
              <FormItem className="flex w-full flex-col gap-3">
                <FormControl className="mt-3.5">
                  <Editor
                    apiKey={process.env.NEXT_PUBLIC_TINY_EDITOR_API_KEY}
                    onBlur={field.onBlur}
                    onEditorChange={(content) => field.onChange(content)}
                    onInit={(evt, editor) => {
                      // @ts-ignore
                      editorRef.current = editor;
                    }}
                    init={{
                      height: 300,
                      plugins: [
                        'advlist',
                        'autolink',
                        'lists',
                        'link',
                        'image',
                        'charmap',
                        'preview',
                        'anchor',
                        'searchreplace',
                        'visualblocks',
                        'codesample',
                        'fullscreen',
                        'insertdatetime',
                        'media',
                        'table',
                        ' ai',
                      ],
                      content_style:
                        'body { font-family: Inter, Arial; font-size: 16px ;}',
                      content_css: theme === 'dark' ? 'dark' : 'light',
                      skin: theme === 'dark' ? 'oxide-dark' : 'oxide',
                      toolbar:
                        'undo redo | ' +
                        'codesample | bold italic forecolor | alignleft aligncenter |' +
                        'alignright alignjustify | bullist numlist',
                      tinycomments_mode: 'embedded',
                      tinycomments_author: 'Author name',
                      mergetags_list: [
                        { value: 'First.Name', title: 'First Name' },
                        { value: 'Email', title: 'Email' },
                      ],
                      ai_request: (
                        request: any,
                        respondWith: {
                          string: (arg0: () => Promise<never>) => any;
                        }
                      ) =>
                        respondWith.string(() =>
                          Promise.reject(
                            new Error('See docs to implement AI Assistant')
                          )
                        ),
                    }}
                    initialValue=""
                  />
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />
          <div className="flex justify-end">
            <Button
              type="submit"
              className="primary-gradient min-h-[46px] px-4 py-3 !text-light-900 "
            >
              {form.formState.isSubmitting ? 'Posting...' : 'Post Answer'}
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
};

export default Answer;
