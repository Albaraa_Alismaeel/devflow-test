'use client';
import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { createQuestion, updateQuestion } from '@/lib/actions/question.action';
import { questionSchema } from '@/lib/validation';
import { zodResolver } from '@hookform/resolvers/zod';
import { Editor } from '@tinymce/tinymce-react';
import { useTheme } from 'next-themes';
import Image from 'next/image';
import { usePathname, useRouter } from 'next/navigation';
import React from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { Badge } from '../ui/badge';

const Question = ({
  userId,
  type = 'create',
  question,
}: {
  userId: string;
  type?: 'edit' | 'create';
  question?: string;
}) => {
  const router = useRouter();
  const pathname = usePathname();
  const { theme } = useTheme();

  const questionDetails = JSON.parse(question || '{}');
  const groupedTags = questionDetails.tags?.map((tag: any) => tag.name);

  const form = useForm<z.infer<typeof questionSchema>>({
    resolver: zodResolver(questionSchema),
    defaultValues: {
      title: questionDetails?.title || '',
      explanation: questionDetails?.content || '',
      tags: groupedTags || [],
    },
  });

  async function onSubmit(values: z.infer<typeof questionSchema>) {
    try {
      if (type === 'create') {
        await createQuestion({
          title: values.title,
          content: values.explanation,
          tags: values.tags,
          author: JSON.parse(userId),
          path: pathname,
        });
        router.push('/');
      }
      if (type === 'edit') {
        await updateQuestion({
          questionId: questionDetails._id,
          title: values.title,
          content: values.explanation,
          path: pathname,
        });
        router.push(`/question/${questionDetails._id}`);
      }
    } catch (e) {
      throw new Error('Something went wrong');
    }
  }

  const handleKeyDown = (
    e: React.KeyboardEvent<HTMLInputElement>,
    field: any
  ) => {
    if (e.key === 'Enter' && field.name === 'tags') {
      e.preventDefault();
      const tagInp = e.target as HTMLInputElement;
      const tagVal = tagInp.value.trim();
      if (tagVal !== '')
        if (tagVal.length > 15)
          return form.setError('tags', {
            message: 'Tag length should be less than 15 characters',
          });

      if (!field.value.includes(tagVal as never)) {
        form.setValue('tags', [...field.value, tagVal]);
        tagInp.value = '';
        form.clearErrors('tags');
      } else {
        form.trigger();
      }
    }
  };
  const handleTagRemove = (tag: string, field: any) => {
    const newTags = field.value.filter((t: string) => t !== tag);
    form.setValue('tags', newTags);
  };
  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="flex w-full flex-col gap-10"
      >
        <FormField
          control={form.control}
          name="title"
          render={({ field }) => (
            <FormItem className="flex w-full flex-col">
              <FormLabel className="paragraph-semibold text-dark400_light800">
                Question Title <span className="text-primary-500">*</span>
              </FormLabel>
              <FormControl className="mt-3.5">
                <Input
                  className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none shadow-none outline-none"
                  {...field}
                />
              </FormControl>
              <FormDescription className="body-regular mt-2.5 text-light-500">
                Be specific and imagine you’re asking a question to another
                person.
              </FormDescription>
              <FormMessage className="text-red-500" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="explanation"
          render={({ field }) => (
            <FormItem className="flex w-full flex-col gap-3">
              <FormLabel className="paragraph-semibold text-dark400_light800">
                Detailed explanation of your problem{' '}
                <span className="text-primary-500">*</span>
              </FormLabel>
              <FormControl className="mt-3.5">
                <Editor
                  apiKey={process.env.NEXT_PUBLIC_TINY_EDITOR_API_KEY}
                  onBlur={field.onBlur}
                  onEditorChange={(content) => field.onChange(content)}
                  init={{
                    height: 300,
                    plugins: [
                      'advlist',
                      'autolink',
                      'lists',
                      'link',
                      'image',
                      'charmap',
                      'preview',
                      'anchor',
                      'searchreplace',
                      'visualblocks',
                      'codesample',
                      'fullscreen',
                      'insertdatetime',
                      'media',
                      'table',
                      ' ai',
                    ],
                    content_style:
                      'body { font-family: Inter, Arial; font-size: 16px ;}',
                    content_css: theme === 'dark' ? 'dark' : 'light',
                    skin: theme === 'dark' ? 'oxide-dark' : 'oxide',
                    toolbar:
                      'undo redo | ' +
                      'codesample | bold italic forecolor | alignleft aligncenter |' +
                      'alignright alignjustify | bullist numlist',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                    mergetags_list: [
                      { value: 'First.Name', title: 'First Name' },
                      { value: 'Email', title: 'Email' },
                    ],
                    ai_request: (
                      request: any,
                      respondWith: {
                        string: (arg0: () => Promise<never>) => any;
                      }
                    ) =>
                      respondWith.string(() =>
                        Promise.reject(
                          new Error('See docs to implement AI Assistant')
                        )
                      ),
                  }}
                  initialValue={field.value}
                />
              </FormControl>
              <FormDescription className="body-regular mt-2.5 text-light-500">
                Introduce the problem and expand on what you put in the title.
                Minimum 20 characters.
              </FormDescription>
              <FormMessage className="text-red-500" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="tags"
          render={({ field }) => (
            <FormItem className="flex w-full flex-col">
              <FormLabel className="paragraph-semibold text-dark400_light800">
                Tags <span className="text-primary-500">*</span>
              </FormLabel>
              <FormControl className="mt-3.5">
                <>
                  <Input
                    className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none shadow-none outline-none"
                    placeholder="Add tags..."
                    disabled={type === 'edit'}
                    onKeyDown={(e) => handleKeyDown(e, field)}
                  />
                  {field.value.length > 0 && (
                    <div className="mt-2.5 flex gap-2.5">
                      {field.value.map((tag, index) => (
                        <Badge
                          key={index}
                          className="subtle-medium background-light800_dark300 text-light400_light500 flex cursor-pointer select-none items-center justify-center gap-2 rounded-md border-none px-4 py-2 capitalize"
                          onClick={() => {
                            if (type !== 'edit') handleTagRemove(tag, field);
                          }}
                        >
                          <span>{tag}</span>
                          {type !== 'edit' && (
                            <Image
                              src="/assets/icons/close.svg"
                              alt="close"
                              width={12}
                              height={12}
                              className="cursor-pointer object-contain invert-0 dark:invert"
                            />
                          )}
                        </Badge>
                      ))}
                    </div>
                  )}
                </>
              </FormControl>
              <FormDescription className="body-regular mt-2.5 text-light-500">
                Add up to 5 tags to describe what your question is about. Start
                typing to see suggestions.
              </FormDescription>
              <FormMessage className="text-red-500" />
            </FormItem>
          )}
        />
        <Button
          type="submit"
          className="primary-gradient w-fit !text-light-900"
          disabled={form.formState.isSubmitting}
        >
          {form.formState.isSubmitting
            ? type === 'create'
              ? 'Posting...'
              : 'Editing...'
            : type === 'create'
              ? 'Ask a question'
              : 'Edit Question'}
        </Button>
      </form>
    </Form>
  );
};

export default Question;
