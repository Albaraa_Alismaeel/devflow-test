'use client';
import { updateUser } from '@/lib/actions/user.action';
import { profileSchema } from '@/lib/validation';
import { zodResolver } from '@hookform/resolvers/zod';
import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { Button } from '../ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '../ui/form';
import { Input } from '../ui/input';
import { Textarea } from '../ui/textarea';

type UserProps = {
  clerkId: string;
  name: string;
  username: string;
  portfolio?: string;
  location?: string;
  bio?: string;
};

const ProfileForm = ({
  clerkId,
  name,
  username,
  portfolio,
  location,
  bio,
}: UserProps) => {
  const route = useRouter();
  const form = useForm<z.infer<typeof profileSchema>>({
    resolver: zodResolver(profileSchema),
    defaultValues: {
      name: name || '',
      username: username || '',
      portfolio: portfolio || '',
      location: location || '',
      bio: bio || '',
    },
  });

  async function onSubmit(values: z.infer<typeof profileSchema>) {
    await updateUser({
      clerkId,
      updateData: {
        name: values.name,
        username: values.username,
        location: values.location,
        portfolioWebsite: values.portfolio,
        bio: values.bio,
      },
      path: `/profile/${clerkId}`,
    });
    route.push(`/profile/${clerkId}`);
  }

  return (
    <div>
      <Form {...form}>
        <form
          className="flex w-full flex-col gap-10"
          onSubmit={form.handleSubmit(onSubmit)}
        >
          <FormField
            name="name"
            control={form.control}
            render={({ field }) => (
              <FormItem className="flex w-full flex-col">
                <FormLabel className="paragraph-semibold text-dark400_light800">
                  Full Name <span className="text-primary-500">*</span>
                </FormLabel>
                <FormControl className="mt-3.5">
                  <Input
                    className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none shadow-none outline-none"
                    {...field}
                    placeholder="John Doe"
                  />
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />
          <FormField
            name="username"
            control={form.control}
            render={({ field }) => (
              <FormItem className="flex w-full flex-col">
                <FormLabel className="paragraph-semibold text-dark400_light800">
                  Username <span className="text-primary-500">*</span>
                </FormLabel>
                <FormControl className="mt-3.5">
                  <div className="relative">
                    <span className="absolute left-2 top-1/2 -translate-y-1/2 text-[#858EAD] dark:text-[#7B8DC6]">
                      @
                    </span>
                    <Input
                      className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none ps-7 shadow-none outline-none"
                      {...field}
                      placeholder="username"
                    />
                  </div>
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />
          <FormField
            name="portfolio"
            control={form.control}
            render={({ field }) => (
              <FormItem className="flex w-full flex-col">
                <FormLabel className="paragraph-semibold text-dark400_light800">
                  Protfolio Link
                </FormLabel>
                <FormControl className="mt-3.5">
                  <Input
                    className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none shadow-none outline-none"
                    {...field}
                    placeholder="https://www.example.com"
                  />
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />
          <FormField
            name="location"
            control={form.control}
            render={({ field }) => (
              <FormItem className="flex w-full flex-col">
                <FormLabel className="paragraph-semibold text-dark400_light800">
                  Location
                </FormLabel>
                <FormControl className="mt-3.5">
                  <Input
                    className="paragraph-regular no-focus placeholder background-light800_dark300 min-h-[56px] border-none shadow-none outline-none"
                    {...field}
                    placeholder="Croatia, Europe"
                  />
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />
          <FormField
            name="bio"
            control={form.control}
            render={({ field }) => (
              <FormItem className="flex w-full flex-col">
                <FormLabel className="paragraph-semibold text-dark400_light800">
                  Bio
                </FormLabel>
                <FormControl className="mt-3.5">
                  <Textarea
                    className="paragraph-regular no-focus placeholder background-light800_dark300  border-none shadow-none outline-none"
                    {...field}
                    placeholder="Talk about yourself..."
                  />
                </FormControl>
                <FormMessage className="text-red-500" />
              </FormItem>
            )}
          />

          <div className="flex justify-end">
            <Button
              type="submit"
              className="primary-gradient min-h-[46px] px-4 py-3 !text-light-900"
            >
              {form.formState.isSubmitting ? 'Saving...' : 'Save Changes'}
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
};

export default ProfileForm;
